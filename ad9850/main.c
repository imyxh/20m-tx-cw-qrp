#include <stdint.h>

#include "nrf.h"
#include "nrf_drv_usbd.h"
#include "nrf_drv_clock.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_power.h"

#include "app_error.h"
#include "app_util.h"
#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_serial_num.h"

#include "boards.h"

// LED pin number
#define LED_RED 22
#define LED_GREEN 23
#define LED_BLUE 24

// map inputs to AD9850 from nRF52840 pins
#define D7 7
#define RESET 4
#define FQ_UD 5
#define W_CLK 6


/**
 * @brief broadcast at f_out MHz for usec_on ms
 *
 * The formula used by the AD9850 is as follows:
 *     f_out = (ftword * f_clock) / 2^32
 * where:
 *   - ftword is the value of the 32-bit frequency-tuning word,
 *   - f_clock is the frequency of the input reference clock, and
 *   - f_out is the output frequency in MHz.
 *
 * @see https://www.analog.com/media/en/technical-documentation/data-sheets/AD9850.pdf
 * @param f_out desired frequency to tune to, in kHz
 * @param f_clock frequency of input reference clock, in MHz
 * @return frequency-tuning word (uint32_t)
 */
void tx(int f_out, int f_clock, int msec_on)
{
	char reg[5];
	// first 32 bits: ftword
	double ftword = f_out / 1000.0 * 4294967296 / f_clock;
	*(uint32_t *)reg = (uint32_t)(ftword + 0.5);
	// next 8 bits: control, powerdown, phase (all null)
	reg[4] = 0;
	
	nrf_gpio_pin_clear(FQ_UD);
	nrf_delay_ms(1);
	for (int i = 0; i < 40; i++) {
		uint32_t bit = reg[i / 8] >> (i % 8)
		bit &= 1;
		nrf_gpio_pin_write(D7, bit);
		nrf_delay_ms(1);
		nrf_gpio_pin_set(W_CLK);
		nrf_delay_ms(1);
		nrf_gpio_pin_clear(W_CLK);
		nrf_delay_ms(1);
	} 
	nrf_gpio_pin_set(FQ_UD);
	nrf_delay_ms(1);
	nrf_gpio_pin_clear(FQ_UD);

	nrf_gpio_pin_set(LED_BLUE);	// blue means txing
	nrf_delay_ms(msec_on);
	nrf_gpio_pin_clear(LED_BLUE);

	// power-down
	for (int i = 0; i < 40; i++) {
		if (i == 34) {
			nrf_gpio_pin_set(D7);
		} else {
			nrf_gpio_pin_clear(D7);
		}
		nrf_delay_ms(1);
		nrf_gpio_pin_set(W_CLK);
		nrf_delay_ms(1);
		nrf_gpio_pin_clear(W_CLK);
		nrf_delay_ms(1);
	} 
	nrf_gpio_pin_set(FQ_UD);
	nrf_delay_ms(1);
	nrf_gpio_pin_clear(FQ_UD);

	return;
}


int main()
{

	// configure pins as outputs
	nrf_gpio_cfg_output(LED_RED);
	nrf_gpio_cfg_output(LED_GREEN);
	nrf_gpio_cfg_output(LED_BLUE);
	nrf_gpio_cfg_output(D7);
	nrf_gpio_cfg_output(RESET);
	nrf_gpio_cfg_output(FQ_UD);
	nrf_gpio_cfg_output(W_CLK);

	while (1) {
		// broadcast at 21088 kHz, clock input of 125 MHz, for 1000 ms
		tx(21088, 125, 1000);
		nrf_delay_ms(1000);
	}
}

