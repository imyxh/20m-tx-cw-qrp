AD9850-based 21 MHz transmitter
===============================

A project in progress to learn about AC circuits and radio-frequency
electronics. This is written for the nRF52840 USB dongle, but could be ported to
other devices.

![schematic](/hardware/AD9850-21MHz-TX.svg "hardware schematic")

Building
--------

Some configuration is needed. Edit components/toolchain/gcc/Makefile.posix under
the appropriate nRF SDK folder to include gcc properly via a hard path (dumb, I
know; fixing this is TODO).

```Makefile
GNU_INSTALL_ROOT ?= ${HOME}/git/AD9850-21MHz-TX/gcc-arm-none-eabi-6-2017-q2-update/arm-none-eabi/bin/
```

Also, you'll need to download nRF SDK (15.0.0\_a53641a), the [mergehex] tool,
and gcc-arm-none-eabi-6-2017-q2-update. Place them in the following directories
(relative to this README):

```
./nrf_sdks/nRF5_SDK_15.0.0_a53641a
./mergehex
./gcc-arm-none-eabi-6-2017-q2-update
```

Example compilation and build:

```sh
cd /home/imyxh/playground/nrf/nrf52840-mdk-usb-dongle/examples/nrf5-sdk/blinky/armgcc
make && cd _build
nrfutil pkg generate --hw-version 52 --sd-req 0x00 --application-version 1 --application nrf52840_xxaa.hex blinky_dfu_package.zip
```

To flash the package, connect the dongle to USB while holding down the button.
One of the LEDs should pulse bright red, and the device should show up under
lsusb. Then:

```sh
sudo nrfutil dfu usb-serial -pkg blinky_dfu_package.zip -snr C6C995596247
```

For this project, I've provided a Makefile:

```sh
make install SNR=C6C995596247
```

Installing will require sudo, as you need to write to the dongle. You can also
run Make with no arguments to build, but not flash.

The nrfutil tool is installed via a Python virtual environment named "nrf" and
there's a local taskrc set for this project. I had the brilliant idea of
including an "env.sh" file for you to source that will load both the nrf
environment and also alias "task" to run Taskwarrior with the local taskrc.

```sh
source ./env.sh
```

I'll see if I like this sort of configuration. Might be useful for future
projects as well.


How it works
------------

uh


<!-- footer -->
[mergehex]: https://github.com/makerdiary/nrf52840-mdk-usb-dongle/tree/master/tools/mergehex

