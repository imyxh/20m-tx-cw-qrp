build:
	cd ad9850/armgcc && make
	source ./env.sh && cd ad9850/armgcc/_build && nrfutil pkg generate \
		--hw-version 52 \
		--sd-req 0x00 \
		--application-version 1 \
		--application nrf52840_xxaa.hex \
		ad9850_dfu_pkg.zip
	mv ad9850/armgcc/_build/ad9850_dfu_pkg.zip .

install: build
	sudo nrfutil dfu usb-serial -pkg ad9850_dfu_pkg.zip -snr $(SNR)

